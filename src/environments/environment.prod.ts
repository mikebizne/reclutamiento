export const environment = {
  production: true,
  apiUrl: 'https://venados.dacodes.mx/api',
  apiLocal: '/assets/data/',
};
