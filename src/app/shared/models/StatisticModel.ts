export class StatisticModel {
  position: number;
  image: string;
  team: string;
  games: number;
  win: number;
  loss: number;
  tie: number;
  fGoals: number;
  aGoals: number;
  scoreDiff: number;
  points: number;
  effectiveness: number;

  constructor() {
    this.position = 0;
    this.image = null;
    this.team = null;
    this.games = 0;
    this.win = 0;
    this.loss = 0;
    this.tie = 0;
    this.fGoals = 0;
    this.aGoals = 0;
    this.scoreDiff = 0;
    this.points = 0;
    this.effectiveness = 0;
  }
}
