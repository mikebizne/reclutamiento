import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'filterGames'
})
export class FilterGamesPipe implements PipeTransform {

  transform(items: any, search: string): any {
    if (!search) {
      return items;
    }
    return items.filter(it => {
      return it.league.includes(search);
    });
  }

}
