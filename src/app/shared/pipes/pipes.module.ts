import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FilterGamesPipe} from './filter-games.pipe';
import {GroupGamesPipe} from '@app/shared/pipes/group-games.pipe';

@NgModule({
  declarations: [FilterGamesPipe, GroupGamesPipe],
  imports: [
    CommonModule
  ],
  exports: [
    FilterGamesPipe,
    GroupGamesPipe
  ]
})
export class PipesModule {
}
