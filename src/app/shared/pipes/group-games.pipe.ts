import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'groupGames'
})
export class GroupGamesPipe implements PipeTransform {
  transform(items: any, property: string): any {
    var groups = items.reduce(function (obj, item: any) {
      obj[item.month] = obj[item.month] || [];
      obj[item.month].push(item);
      return obj;
    }, {});

    return Object.keys(groups).map(function (key) {
      return {month: key, games: groups[key]};
    });
  }
}
