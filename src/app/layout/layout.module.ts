import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {LayoutComponent} from '@app/layout/layout.component';
import {SidebarComponent} from '@app/layout/sidebar/sidebar.component';
import {RouterModule} from '@angular/router';
import {NavbarComponent} from "@app/layout/navbar/navbar.component";


@NgModule({
  declarations: [
    LayoutComponent,
    SidebarComponent,
    NavbarComponent
  ],
  imports: [
    CommonModule,
    RouterModule
  ]
})
export class LayoutModule {
}
