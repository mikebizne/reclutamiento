import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {LayoutComponent} from '@app/layout/layout.component';
import {MenuRoutingModule} from '@app/routes/menu-routing.module';

const routes: Routes = [

  {path: '', redirectTo: '/inicio/games', pathMatch: 'full'}];


@NgModule({
  imports: [
    RouterModule.forRoot(routes),
    MenuRoutingModule
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
