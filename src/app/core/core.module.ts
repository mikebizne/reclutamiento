import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ApiService} from '@app/core/http/api.service';
import {VenadosService} from '@app/core/service/venados.service';
import {HttpInterceptor} from '@app/core/interceptor/http.interceptor';
import {HTTP_INTERCEPTORS} from '@angular/common/http';
import {DateService} from '@app/core/utils/date.service';

@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ],
  providers: [
    ApiService,
    VenadosService,
    DateService,
    {provide: HTTP_INTERCEPTORS, useClass: HttpInterceptor, multi: true},
  ]
})
export class CoreModule {
}
