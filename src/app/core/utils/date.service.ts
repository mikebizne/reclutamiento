import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DateService {

  constructor() {
  }


  static setDateName(date) {
    switch (date) {
      case 'Monday': {
        return 'Lunes';
        break;
      }
      case 'Tuesday': {
        return 'Martes';
        break;
      }
      case 'Wednesday': {
        return 'Miércoles';
        break;
      }
      case 'Thursday': {
        return 'Jueves';
        break;
      }
      case 'Friday': {
        return 'Viernes';
        break;
      }
      case 'Saturday': {
        return 'Sábado';
        break;
      }
      case 'Sunday': {
        return 'Domingo';
        break;
      }

    }
  }
}
