import {Injectable} from '@angular/core';
import {ApiService} from '../http/api.service';

@Injectable({
  providedIn: 'root'
})
export class VenadosService {
  endPoint = '';

  constructor(public apiService: ApiService) {

    if (this.apiService.local) {
      this.endPoint = '.json';
    }
  }

  games(params?) {
    return this.apiService.get('games' + this.endPoint);
  }

  statistics(params?) {
    return this.apiService.get('statistics' + this.endPoint);
  }

  players(params?) {
    return this.apiService.get('players' + this.endPoint);
  }

  sponsors(params?) {
    return this.apiService.get('sponsors' + this.endPoint);
  }

  notifications(params?) {
    return this.apiService.get('notifications' + this.endPoint);
  }
}
