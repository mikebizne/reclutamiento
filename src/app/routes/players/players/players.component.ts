import {Component, OnInit} from '@angular/core';
import {VenadosService} from '@app/core/service/venados.service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {DetailsComponent} from '@app/routes/players/details/details.component';

@Component({
  selector: 'app-players',
  templateUrl: './players.component.html',
  styleUrls: ['./players.component.scss']
})
export class PlayersComponent implements OnInit {
  team = {
    forwards: [],
    centers: [],
    defenses: [],
    goalkeepers: [],
    coaches: [],
  };

  constructor(public venadosService: VenadosService,
              public serviceModal: NgbModal) {
  }

  ngOnInit() {
    this.getPlayers();
  }

  getPlayers() {
    this.venadosService.players().subscribe((resp: any) => {
      this.team = resp.data.team;
      console.log(this.team);
    }, (error) => {
      console.log(error);
    });
  }

  getNameFull(player) {
    return player.name + ' ' + player.first_surname + ' ' + player.second_surname;
  }

  openModal(player) {
    const modal = this.serviceModal.open(DetailsComponent);
    modal.componentInstance.player = player;
  }

}
