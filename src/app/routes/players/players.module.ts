import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {PlayersComponent} from './players/players.component';
import {RouterModule, Routes} from '@angular/router';

const routes: Routes = [
  {path: '', component: PlayersComponent},
];

@NgModule({
  declarations: [PlayersComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ]
})
export class PlayersModule {
}
