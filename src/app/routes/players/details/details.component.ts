import {Component, Input, OnInit} from '@angular/core';
import {NgbActiveModal} from "@ng-bootstrap/ng-bootstrap";

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss']
})
export class DetailsComponent implements OnInit {
  modal: any;
  @Input() player: any;

  constructor(public activeModal: NgbActiveModal) {
    this.modal = this.activeModal;
  }

  ngOnInit() {
  }

  getNameFull(player) {
    return player.name + ' ' + player.first_surname + ' ' + player.second_surname;
  }

}
