import {Component, OnInit} from '@angular/core';
import {VenadosService} from '@app/core/service/venados.service';
import {StatisticModel} from '@app/shared/models/StatisticModel';

@Component({
  selector: 'app-statistics',
  templateUrl: './statistics.component.html',
  styleUrls: ['./statistics.component.scss']
})
export class StatisticsComponent implements OnInit {

  statistics: StatisticModel[] = [];

  constructor(public venadosService: VenadosService) {
    this.statistics = [];
  }

  ngOnInit() {
    this.getStatistics();
  }

  getStatistics() {
    this.venadosService.statistics().subscribe((resp: any) => {
      this.statistics = resp.data.statistics;
    }, (error) => {
      console.log(error);
    });
  }

}
