import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {LayoutComponent} from '@app/layout/layout.component';

const routes: Routes = [
  {
    path: 'inicio',
    component: LayoutComponent,
    children: [
      {path: '', redirectTo: 'statistics', pathMatch: 'full'},
      {path: 'games', loadChildren: './games/games.module#GamesModule'},
      {path: 'statistics', loadChildren: './statistics/statistics.module#StatisticsModule'},
      {path: 'players', loadChildren: './players/players.module#PlayersModule'},
    ]
  }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MenuRoutingModule {
}
