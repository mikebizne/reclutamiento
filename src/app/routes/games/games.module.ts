import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {GamesComponent} from './games/games.component';
import {RouterModule, Routes} from "@angular/router";
import {NgbModule} from "@ng-bootstrap/ng-bootstrap";
import { CopaComponent } from './copa/copa.component';
import { AscensoComponent } from './ascenso/ascenso.component';
import {SharedModule} from '@app/shared/shared.module';

const routes: Routes = [
  {path: '', component: GamesComponent},
];

@NgModule({
  declarations: [GamesComponent, CopaComponent, AscensoComponent],
  imports: [
    CommonModule,
    NgbModule,
    RouterModule.forChild(routes),
    SharedModule
  ]
})
export class GamesModule {
}
