import {Component, OnInit} from '@angular/core';
import {NgbTabsetConfig} from '@ng-bootstrap/ng-bootstrap';
import {VenadosService} from "@app/core/service/venados.service";

import * as moment from 'moment';

@Component({
  selector: 'app-games',
  templateUrl: './games.component.html',
  styleUrls: ['./games.component.scss'],
  providers: [NgbTabsetConfig]
})

export class GamesComponent implements OnInit {

  games = [];

  constructor(config: NgbTabsetConfig, public venadosService: VenadosService) {
    config.justify = 'center';
  }

  ngOnInit() {
    this.getGames();
  }

  getGames() {
    this.venadosService.games().subscribe((resp: any) => {
      for (let date of resp.data.games) {
        date['month'] = moment(date.datetime).format('MMMM-Y')
      }
      this.games = resp.data.games;
    }, (error) => {
      console.log(error);
    });
  }

}
