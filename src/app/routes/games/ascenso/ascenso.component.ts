import {Component, Input, OnInit} from '@angular/core';

import * as moment from 'moment';

@Component({
  selector: 'app-ascenso',
  templateUrl: './ascenso.component.html',
  styleUrls: ['./ascenso.component.scss']
})
export class AscensoComponent implements OnInit {
  @Input() games: any;

  constructor() {
  }

  ngOnInit() {
  }

  getDate(date) {
    return moment(date).utcOffset(0).format('ddd')
  }

}
