import {Component, Input, OnInit} from '@angular/core';

import * as moment from 'moment';

@Component({
  selector: 'app-copa',
  templateUrl: './copa.component.html',
  styleUrls: ['./copa.component.scss']
})
export class CopaComponent implements OnInit {

  @Input() games: any;


  constructor() {
  }

  ngOnInit() {

  }

  getDate(date) {
    return moment(date).utcOffset(0).format('ddd')
  }


}
